<!DOCTYPE html>
<html>
<head>
	<title><?php echo $judul; ?></title>

	<style>
		.content {
			width: 600px;
			margin: auto;
		}
		.profile {
					box-shadow: 0px 10px 30px 10px rgba(32, 30, 38, 0.35);
					margin-top: 100px;
					background: rgb(85,228,214);
					background: linear-gradient(0deg, rgba(85,228,214,1) 22%, rgba(41,160,207,1) 78%);

		}
	</style>

<!-- link css boostrap -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<!-- link css -->
</head>
<body style="background-color: #ecf1f2">

<div class="content">
 <div class="container">
   <div class="row">	
 	<table class="table table-bordered table-dark profile">
       <thead class="text-center">
		    <tr>
		      <th colspan="2">BIODATAKU</th>
		    </tr>
       </thead>

  	   <tbody>
	    	<tr>
		      <td>Nama</td>	
		      <td><?php echo $nama; ?></td>
		    </tr>
		    <tr>  
		      <td>Alamat</td>
			  <td><?php echo $alamat; ?></td>
			</tr>  	
			  <td>Jenis Kelamin</td>
			  <td><?php echo $kelamin; ?></td>
			<tr>  
			  <td>Asal Sekolah</td>
			  <td><?php echo $asal; ?></td>
			</tr>
		    </tr>
       </tbody>
    </table>
   </div>
</div>
</div>

<!-- link js -->
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<!-- link js -->
</body>
</html>